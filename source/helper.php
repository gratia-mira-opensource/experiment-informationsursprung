<?php

 // Kein direkter Aufruf der PHP-Datei
 defined('_JEXEC') or die('Restricted Access');

// Hilfsklassen laden
use Joomla\CMS\Factory;

class ExperimentInformation {
	// Die Klasse instanziieren, d.h. ein Objekt erzeugen
    function __construct(&$params) {

    $ArrayABC = range('a','z');
    $ArraySonderzeichen = array(' ');
    $ArrayGrundzeichen = array_merge($ArrayABC,$ArraySonderzeichen);
    
    // Parameter abrufen
    $get = Factory::getApplication();
    $EingabeParameter = $get->input;
    $Information = trim($EingabeParameter->get('Information','', 'String'));

    // Mutation oder Zeichenkette
    if(isset($_POST['InhaltInformation'])) {
        $_SESSION['Runde']++;

        $AlteZeichenkette = trim($EingabeParameter->get('InhaltInformation','', 'String'));
        $ArrayZeichen = str_split($AlteZeichenkette);
        
        // Zufälliges Zeichen dazumischen, wenn der Zufall es will
        if(mt_rand(1,10) == 10) {
            $NeuesZeichen = array($ArrayGrundzeichen[array_rand($ArrayGrundzeichen,1)]);
            $ArrayZeichen = array_merge($NeuesZeichen,$ArrayZeichen);
        }
    } else {
        $_SESSION['Runde'] = 1;
        // Versuche zählen
        if(isset($_SESSION['Versuche'])) {
            $_SESSION['Versuche']++;
        } else {
            $_SESSION['Versuche'] = 1;
        }
        
        // Benutzerinformation oder ABC
        if($Information) {
            $ArrayZeichen = str_split($Information);
        } else {
            $ArrayZeichen = $ArrayGrundzeichen;
        }
    }

    // Zeichenkette erstellen
    $Zeichenkette = '';
    for($i=0; $i < 200; $i++) {
        $Zeichenkette .= $ArrayZeichen[array_rand($ArrayZeichen,1)];
    }

    // Unterschiedliche Zeichen ermitteln
    $ArrayNeueZeichenkette = str_split($Zeichenkette);
    $ArrayAnzahlproBuchstabe = array_count_values($ArrayNeueZeichenkette);
    $AnzahlUnterschedlicheZeichen = count($ArrayAnzahlproBuchstabe);

    // Zusatzinformationen
    $Zusatzinfo = '';
    foreach ($ArrayGrundzeichen as $Buchstabe) {
        $Zusatzinfo .= str_replace(' ', 'Leerzeichen',strtoupper($Buchstabe)) . ': ' . substr_count($Zeichenkette, $Buchstabe) . ';  ';
    }
    
    // Zeichen formatieren
    $Suchen = array('/\b(0)\b/','/\b(1|2|3|4|5)\b/','/\b([5-9][0-9]|[1-9][0-9][0-9])\b/');
    $Ersetzen = array('<span style="color: red;">$1</span>','<span style="color: orange;">$1</span>','<span style="color:green;">$1</span>');
    $Zusatzinfo = preg_replace($Suchen,$Ersetzen,$Zusatzinfo);

    $this->Generation = $_SESSION['Runde'];
    $this->Versuche = $_SESSION['Versuche'];
    $this->Zusatzinfo = $Zusatzinfo . '<br>Unterschiedliche Zeichen: '. $AnzahlUnterschedlicheZeichen;
    $this->GenerierterText = $Zeichenkette; 
    }
}

?>